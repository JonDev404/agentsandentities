using TMPro;
using UnityEngine;

namespace AgentsAndEntities
{
    public class FpsCounter : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI _text;
        
        float updateInterval = 0.5f;
        float accum;
        int frames;
        float timeleft;
        float fps;

        void Awake() => timeleft = updateInterval;

        void Update()
        {
            timeleft -= Time.deltaTime;
            accum += Time.timeScale / Time.deltaTime;
            ++frames;

            if (timeleft <= 0.0)
            {
                fps = (accum / frames);
                timeleft = updateInterval;
                accum = 0.0f;
                frames = 0;
                _text.SetText($"{fps:F2} FPS");
            }
        }
    }
}
