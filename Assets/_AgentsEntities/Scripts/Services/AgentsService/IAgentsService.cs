using System;
using System.Collections.Generic;

namespace AgentsAndEntities
{
    public interface IAgentsService : IService
    {
        event Action<IReadOnlyCollection<Agent>> OnAgentsChanged;
        void Spawn();
        void Despawn();
        void DespawnAll();
    }
}
