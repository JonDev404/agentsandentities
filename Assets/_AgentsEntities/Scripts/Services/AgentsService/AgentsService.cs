using System;
using System.Collections.Generic;

namespace AgentsAndEntities
{
    public class AgentsService : BaseService, IAgentsService
    {
        readonly Queue<Agent> _agents = new();
        public override ServiceInfo Init() => new(typeof(IAgentsService), this);

        public event Action<IReadOnlyCollection<Agent>> OnAgentsChanged;

        public void Spawn()
        {
            var agent = Services.Get<IAgentsPoolingService>().GetObject();
            agent.gameObject.SetActive(true);
            
            _agents.Enqueue(agent);
            
            OnAgentsChanged?.Invoke(_agents);
        }

        public void Despawn()
        {
            if (_agents.Count == 0)
                return;
            
            Services.Get<IAgentsPoolingService>().Return(_agents.Dequeue());
            OnAgentsChanged?.Invoke(_agents);
        }

        public void DespawnAll()
        {
            while(_agents.Count > 0)
                Services.Get<IAgentsPoolingService>().Return(_agents.Dequeue());
            
            OnAgentsChanged?.Invoke(_agents);
        }
    }
}
