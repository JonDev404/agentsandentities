using System;
using System.Collections.Generic;
using UnityEngine;

namespace AgentsAndEntities
{
    public class Services : MonoBehaviour
    {
        static readonly Dictionary<Type, IService> _services = new();

        void Awake()
        {
            var baseServices = Resources.LoadAll<BaseService>(string.Empty);

            foreach (var service in baseServices)
            {
                var serviceInfo = Instantiate(service, transform).Init();
                _services.Add(serviceInfo.Type, serviceInfo.Service);
            }
        }

        public static T Get<T>() where T : IService =>
            _services.TryGetValue(typeof(T), out var service) ? (T)service : default;
    }
}
