using UnityEngine;

namespace AgentsAndEntities
{
    public abstract class BaseService : MonoBehaviour
    {
        public abstract ServiceInfo Init();
    }
}
