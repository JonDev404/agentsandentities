using UnityEngine;

namespace AgentsAndEntities
{
    public interface IWaypointsService : IService
    {
        Waypoints RuntimeWaypoints { get; set; }
        Vector3 GetRandomWaypoint();
    }
}
