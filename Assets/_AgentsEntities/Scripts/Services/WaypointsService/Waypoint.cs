using UnityEngine;

namespace AgentsAndEntities
{
    public class Waypoint : MonoBehaviour
    {
        Vector3 _position;
        
        public Vector3 Position => _position;

        void Awake()
        {
            _position = transform.position;
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, 1f);
        }
    }
}
