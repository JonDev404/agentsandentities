using System;
using UnityEngine;

namespace AgentsAndEntities
{
    public class Waypoints : MonoBehaviour
    {
        [SerializeField] Waypoint[] _waypoints;
        
        public Waypoint this[int index]
        {
            get
            {
                if (index < 0 || index >= _waypoints.Length)
                    return null;

                return _waypoints[index];
            }
        }

        public int Count => _waypoints.Length;

        void Awake()
        {
            Services.Get<IWaypointsService>().RuntimeWaypoints = this;
        }
    }
}
