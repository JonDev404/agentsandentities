using UnityEngine;

namespace AgentsAndEntities
{
    public class WaypointsService : BaseService, IWaypointsService
    {
        public Waypoints RuntimeWaypoints { get; set; }
        public override ServiceInfo Init() => new(typeof(IWaypointsService), this);

        public Vector3 GetRandomWaypoint()
        {
            return RuntimeWaypoints[Random.Range(0, RuntimeWaypoints.Count)].Position;
        }
    }
}
