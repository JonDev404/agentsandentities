using System;

namespace AgentsAndEntities
{
    public class ServiceInfo
    {
        public Type Type { get; }
        public IService Service { get; }

        public ServiceInfo(Type type, IService service)
        {
            Type = type;
            Service = service;
        }
    }
}
