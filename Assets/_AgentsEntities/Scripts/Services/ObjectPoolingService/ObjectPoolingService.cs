using System.Collections.Generic;
using UnityEngine;

namespace AgentsAndEntities
{
    public abstract class ObjectPoolingService<T> : BaseService where T : Component
    {
        [SerializeField] T _prefab;

        readonly Queue<T> _objects = new();

        void AddObject(int count)
        {
            var newObject = Instantiate(_prefab);
            newObject.gameObject.SetActive(false);
            _objects.Enqueue(newObject);
        }
        
        public T GetObject()
        {
            if (_objects.Count == 0)
                AddObject(1);

            return _objects.Dequeue();
        }

        public void Return(T objectToReturn)
        {
            objectToReturn.gameObject.SetActive(false);
            _objects.Enqueue(objectToReturn);
        }
        
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
                Services.Get<IAgentsPoolingService>().GetObject();
        }
    }
}
