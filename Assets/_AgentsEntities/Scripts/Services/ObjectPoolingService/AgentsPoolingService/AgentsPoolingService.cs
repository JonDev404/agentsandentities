namespace AgentsAndEntities
{
    public class AgentsPoolingService : ObjectPoolingService<Agent>, IAgentsPoolingService
    {
        public override ServiceInfo Init() => new(typeof(IAgentsPoolingService), this);
    }
}
