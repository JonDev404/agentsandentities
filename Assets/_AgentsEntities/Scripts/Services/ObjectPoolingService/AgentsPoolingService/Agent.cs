using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace AgentsAndEntities
{
    public class Agent : MonoBehaviour
    {
        void OnEnable()
        {
            MoveToNextPoint();
        }

        void OnDisable()
        {
            transform.DOKill(this);
        }

        void MoveToNextPoint()
        {
            transform.DOMove(Services.Get<IWaypointsService>().GetRandomWaypoint(), Random.Range(1.5f, 5f))
                .OnComplete(MoveToNextPoint);
        }
    }
}
