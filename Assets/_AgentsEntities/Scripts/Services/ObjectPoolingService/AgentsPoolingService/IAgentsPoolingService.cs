namespace AgentsAndEntities
{
    public interface IAgentsPoolingService : IObjectPoolingService<Agent> { }
}
