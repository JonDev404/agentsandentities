namespace AgentsAndEntities
{
    public interface IObjectPoolingService<T> : IService
    {
        T GetObject();
        void Return(T objectToReturn);
    }
}