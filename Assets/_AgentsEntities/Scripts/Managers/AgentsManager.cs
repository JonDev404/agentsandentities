using UnityEngine;

namespace AgentsAndEntities
{
    public class AgentsManager : MonoBehaviour
    {
        IAgentsService _agentsService;

        void Awake()
        {
            _agentsService = Services.Get<IAgentsService>();
        }

        void Update()
        {
            if (Input.GetKey(KeyCode.Alpha1))
                _agentsService.Spawn();
            else if (Input.GetKey(KeyCode.Alpha2))
                _agentsService.Despawn();
            else if (Input.GetKeyDown(KeyCode.Alpha3))
                _agentsService.DespawnAll();
        }
    }
}
