using UnityEngine;

namespace AgentsAndEntities
{
    public class App : MonoBehaviour
    {
        void Update()
        {
            if(Input.GetKeyDown(KeyCode.Escape))
                Application.Quit();
        }
    }
}
