using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace AgentsAndEntities
{
    public class AgentsCounterText : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI _text;
        void OnEnable()
        {
            Services.Get<IAgentsService>().OnAgentsChanged += HandleAgentsChanged;
        }

        void OnDisable()
        {
            Services.Get<IAgentsService>().OnAgentsChanged -= HandleAgentsChanged;
        }

        void HandleAgentsChanged(IReadOnlyCollection<Agent> agents)
        {
            _text.SetText($"{agents.Count} agents");
        }
    }
}
